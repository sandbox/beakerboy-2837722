<?php

/**
 * @file
 * Contains oak.page.inc.
 *
 * Page callback for Oak entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Oak templates.
 *
 * Default template: oak.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_oak(array &$variables) {

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
