<?php

/**
 * @file
 * Contains cistern.page.inc.
 *
 * Page callback for Cistern entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Cistern templates.
 *
 * Default template: cistern.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_cistern(array &$variables) {

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
