<?php

/**
 * @file
 * Contains barrel_lot.page.inc.
 *
 * Page callback for BarrelLot entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for BarrelLot templates.
 *
 * Default template: barrel_lot.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_barrel_lot(array &$variables) {

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
