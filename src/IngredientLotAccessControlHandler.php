<?php

namespace Drupal\batch_system;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Access controller for the IngredientLot entity.
 *
 * @see \Drupal\batch_system\Entity\IngredientLot.
 */
class IngredientLotAccessControlHandler extends EntityAccessControlHandler {

}
