<?php

namespace Drupal\batch_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for IngredientLot entities.
 */
class IngredientLotViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['ingredient_lot']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('IngredientLot'),
      'help' => $this->t('The IngredientLot ID.'),
    );

    return $data;
  }

}
