<?php

namespace Drupal\batch_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Barrel entities.
 */
class BarrelViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['barrel']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Barrel'),
      'help' => $this->t('The Barrel ID.'),
    );

    return $data;
  }

}
