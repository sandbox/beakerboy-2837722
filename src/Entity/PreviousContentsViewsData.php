<?php

namespace Drupal\batch_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for PreviousContents entities.
 */
class PreviousContentsViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['previous_contents']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('PreviousContents'),
      'help' => $this->t('The PreviousContents ID.'),
    );

    return $data;
  }

}
