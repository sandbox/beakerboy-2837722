<?php

namespace Drupal\batch_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the PreviousContents entity.
 *
 * @ingroup batch_system
 *
 * @ContentEntityType(
 *   id = "previous_contents",
 *   label = @Translation("PreviousContents"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\batch_system\PreviousContentsListBuilder",
 *     "views_data" = "Drupal\batch_system\Entity\PreviousContentsViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\batch_system\Form\PreviousContentsForm",
 *       "add" = "Drupal\batch_system\Form\PreviousContentsForm",
 *       "edit" = "Drupal\batch_system\Form\PreviousContentsForm",
 *       "delete" = "Drupal\batch_system\Form\PreviousContentsDeleteForm",
 *     },
 *     "access" = "Drupal\batch_system\PreviousContentsAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\batch_system\PreviousContentsHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "previous_contents",
 *   admin_permission = "administer previous_contents entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "previous_contents",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/batch_system/previous_contents/{previous_contents}",
 *     "add-form" = "/admin/structure/batch_system/previous_contents/add",
 *     "edit-form" = "/admin/structure/batch_system/previous_contents/{previous_contents}/edit",
 *     "delete-form" = "/admin/structure/batch_system/previous_contents/{previous_contents}/delete",
 *     "collection" = "/admin/structure/batch_system/previous_contents",
 *   },
 *   field_ui_base_route = "previous_contents.settings"
 * )
 */
class PreviousContents extends ContentEntityBase implements PreviousContentsInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPreviousContents() {
    return $this->get('previous_contents')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPreviousContents($name) {
    $this->set('previous_contents', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Previous Contents'))
      ->setDescription(t('The previous contents in the barrel.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
