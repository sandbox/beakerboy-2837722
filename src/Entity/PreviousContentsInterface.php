<?php

namespace Drupal\batch_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining PreviousContents entities.
 *
 * @ingroup batch_system
 */
interface PreviousContentsInterface extends ContentEntityInterface {

  /**
   * Gets the PreviousContents name.
   *
   * @return string
   *   Name of the PreviousContents.
   */
  public function getPreviousContents();

  /**
   * Sets the PreviousContents name.
   *
   * @param string $name
   *   The PreviousContents name.
   *
   * @return \Drupal\batch_system\Entity\PreviousContentsInterface
   *   The called PreviousContents entity.
   */
  public function setPreviousContents($name);

}
