<?php

namespace Drupal\batch_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Oak entity.
 *
 * @ingroup batch_system
 *
 * @ContentEntityType(
 *   id = "oak",
 *   label = @Translation("Oak"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\batch_system\OakListBuilder",
 *     "views_data" = "Drupal\batch_system\Entity\OakViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\batch_system\Form\OakForm",
 *       "add" = "Drupal\batch_system\Form\OakForm",
 *       "edit" = "Drupal\batch_system\Form\OakForm",
 *       "delete" = "Drupal\batch_system\Form\OakDeleteForm",
 *     },
 *     "access" = "Drupal\batch_system\OakAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\batch_system\OakHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "oak",
 *   admin_permission = "administer oak entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "description" = "description",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/batch_system/oak/{oak}",
 *     "add-form" = "/admin/structure/batch_system/oak/add",
 *     "edit-form" = "/admin/structure/batch_system/oak/{oak}/edit",
 *     "delete-form" = "/admin/structure/batch_system/oak/{oak}/delete",
 *     "collection" = "/admin/structure/batch_system/oak",
 *   },
 *   field_ui_base_route = "oak.settings"
 * )
 */
class Oak extends ContentEntityBase implements BatchSystemInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Oak.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Oak description.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
