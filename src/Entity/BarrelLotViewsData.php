<?php

namespace Drupal\batch_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for BarrelLot entities.
 */
class BarrelLotViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['barrel_lot']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('BarrelLot'),
      'help' => $this->t('The BarrelLot ID.'),
    );

    return $data;
  }

}
