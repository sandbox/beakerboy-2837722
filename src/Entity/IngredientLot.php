<?php

namespace Drupal\batch_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the IngredientLot entity.
 *
 * @ingroup batch_system
 *
 * @ContentEntityType(
 *   id = "ingredient_lot",
 *   label = @Translation("IngredientLot"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\batch_system\IngredientLotListBuilder",
 *     "views_data" = "Drupal\batch_system\Entity\IngredientLotViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\batch_system\Form\IngredientLotForm",
 *       "add" = "Drupal\batch_system\Form\IngredientLotForm",
 *       "edit" = "Drupal\batch_system\Form\IngredientLotForm",
 *       "delete" = "Drupal\batch_system\Form\IngredientLotDeleteForm",
 *     },
 *     "access" = "Drupal\batch_system\IngredientLotAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\batch_system\IngredientLotHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "ingredient_lot",
 *   admin_permission = "administer ingredient_lot entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "sample_id" = "sample_id",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/batch_system/ingredient_lot/{ingredient_lot}",
 *     "add-form" = "/admin/structure/batch_system/ingredient_lot/add",
 *     "edit-form" = "/admin/structure/batch_system/ingredient_lot/{ingredient_lot}/edit",
 *     "delete-form" = "/admin/structure/batch_system/ingredient_lot/{ingredient_lot}/delete",
 *     "collection" = "/admin/structure/batch_system/ingredient_lot",
 *   },
 *   field_ui_base_route = "ingredient_lot.settings"
 * )
 */
class IngredientLot extends ContentEntityBase implements BatchSystemInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Lot'))
      ->setDescription(t('The lot number of the IngredientLot.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['note'] = BaseFieldDefinition::create('text')
      ->setLabel(t('Note'))
      ->setDescription(t('Add a note.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'text',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'textarea',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['ingredient'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Ingredient'))
      ->setSettings(array(
        'target_type' => 'ingredient',
      ))
      ->setDescription(t('Ingredient Type'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'above',
        'type' => 'entity_reference_autocomplete',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['sample_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Analysis Samples'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings(array(
        'target_type' => 'sample',
      ))
      ->setDescription(t('Samples for Analysis.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'above',
        'type' => 'entity_reference_autocomplete',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    return $fields;
  }

}
