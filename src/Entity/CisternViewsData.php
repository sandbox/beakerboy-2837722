<?php

namespace Drupal\batch_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Cistern entities.
 */
class CisternViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['cistern']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Cistern'),
      'help' => $this->t('The Cistern ID.'),
    );

    return $data;
  }

}
