<?php

namespace Drupal\batch_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Fermentor entity.
 *
 * @ingroup batch_system
 *
 * @ContentEntityType(
 *   id = "fermentor",
 *   label = @Translation("Fermentor"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\batch_system\FermentorListBuilder",
 *     "views_data" = "Drupal\batch_system\Entity\FermentorViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\batch_system\Form\FermentorForm",
 *       "add" = "Drupal\batch_system\Form\FermentorForm",
 *       "edit" = "Drupal\batch_system\Form\FermentorForm",
 *       "delete" = "Drupal\batch_system\Form\FermentorDeleteForm",
 *     },
 *     "access" = "Drupal\batch_system\FermentorAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\batch_system\FermentorHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "fermentor",
 *   admin_permission = "administer fermentor entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "description" = "description",
 *     "date" = "date",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/batch_system/fermentor/{fermentor}",
 *     "add-form" = "/admin/structure/batch_system/fermentor/add",
 *     "edit-form" = "/admin/structure/batch_system/fermentor/{fermentor}/edit",
 *     "delete-form" = "/admin/structure/batch_system/fermentor/{fermentor}/delete",
 *     "collection" = "/admin/structure/batch_system/fermentor",
 *   },
 *   field_ui_base_route = "fermentor.settings"
 * )
 */
class Fermentor extends ContentEntityBase implements BatchSystemInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['yeast_temperature'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Yeast Temp'))
      ->setDescription(t('The temperature the yeast was added.'))
      ->setSettings(array(
        'precision' => 5,
        'scale' => 2,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['set_temperature'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Set Temp'))
      ->setDescription(t('The temperature the fermentor was set at.'))
      ->setSettings(array(
        'precision' => 5,
        'scale' => 2,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['beer_temperature'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Beer Temp'))
      ->setDescription(t('The temperature of the fermentor when batch completed.'))
      ->setSettings(array(
        'precision' => 5,
        'scale' => 2,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['yeast_amount'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Yeast Amount.'))
      ->setDescription(t('The pounds of yeast added.'))
      ->setSettings(array(
        'precision' => 4,
        'scale' => 2,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['set_level'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Set Level'))
      ->setDescription(t('Dry inches at set.'))
      ->setSettings(array(
        'precision' => 5,
        'scale' => 2,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['finishing_level'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Finished Level'))
      ->setDescription(t('Dry inches when complete.'))
      ->setSettings(array(
        'precision' => 5,
        'scale' => 2,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['bushels'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Bushels'))
      ->setDescription(t('Bushels of grain that went into the fermentor.'))
      ->setSettings(array(
        'precision' => 6,
        'scale' => 2,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Lot'))
      ->setDescription(t('Unique Lot Number.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['initials'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Initials'))
      ->setDescription(t('Initials of employee who started the fermentor.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['number_cooks'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Number of Cooks'))
      ->setDescription(t('How many cooks went into this fermentor.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['fermentor_number'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Fermentor Number'))
      ->setDescription(t('Which fermentor was filled.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ingredient_lot'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Ingredient Lot'))
      ->setDescription(t('Lot of ingredients used in this fermentor.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings(array(
        'target_type' => 'ingredient_lot',
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['recipe'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Recipe'))
      ->setDescription(t('Grain Recipe.'))
      ->setSettings(array(
        'target_type' => 'recipe',
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['date'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Yeast DateTime'))
      ->setDescription(t('Date and time yeasting began.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'datetime_timestamp',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['set_datetime'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Set DateTime'))
      ->setDescription(t('Date and time fermentor was set.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'datetime_timestamp',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['beer_datetime'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Beer DateTime'))
      ->setDescription(t('Date and time Fermentor was completed.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'datetime_timestamp',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['sample'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Analysis Samples'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'sample')
      ->setDescription(t('Samples for Analysis.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'above',
        'type' => 'entity_reference_autocomplete',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    return $fields;
  }

}
