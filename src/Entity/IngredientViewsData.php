<?php

namespace Drupal\batch_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Ingredient entities.
 */
class IngredientViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['ingredient']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Ingredient'),
      'help' => $this->t('The Ingredient ID.'),
    );

    return $data;
  }

}
