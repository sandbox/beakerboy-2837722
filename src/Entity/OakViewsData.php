<?php

namespace Drupal\batch_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Oak entities.
 */
class OakViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['oak']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Oak'),
      'help' => $this->t('The Oak ID.'),
    );

    return $data;
  }

}
