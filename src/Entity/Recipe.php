<?php

namespace Drupal\batch_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Recipe entity.
 *
 * @ingroup batch_system
 *
 * @ContentEntityType(
 *   id = "recipe",
 *   label = @Translation("Recipe"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\batch_system\RecipeListBuilder",
 *     "views_data" = "Drupal\batch_system\Entity\RecipeViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\batch_system\Form\RecipeForm",
 *       "add" = "Drupal\batch_system\Form\RecipeForm",
 *       "edit" = "Drupal\batch_system\Form\RecipeForm",
 *       "delete" = "Drupal\batch_system\Form\RecipeDeleteForm",
 *     },
 *     "access" = "Drupal\batch_system\RecipeAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\batch_system\RecipeHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "recipe",
 *   admin_permission = "administer recipe entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "description" = "description",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/batch_system/recipe/{recipe}",
 *     "add-form" = "/admin/structure/batch_system/recipe/add",
 *     "edit-form" = "/admin/structure/batch_system/recipe/{recipe}/edit",
 *     "delete-form" = "/admin/structure/batch_system/recipe/{recipe}/delete",
 *     "collection" = "/admin/structure/batch_system/recipe",
 *   },
 *   field_ui_base_route = "recipe.settings"
 * )
 */
class Recipe extends ContentEntityBase implements BatchSystemInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Recipe.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Recipe description.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
