<?php

namespace Drupal\batch_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Ingredient entity.
 *
 * @ingroup batch_system
 *
 * @ContentEntityType(
 *   id = "ingredient",
 *   label = @Translation("Ingredient"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\batch_system\IngredientListBuilder",
 *     "views_data" = "Drupal\batch_system\Entity\IngredientViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\batch_system\Form\IngredientForm",
 *       "add" = "Drupal\batch_system\Form\IngredientForm",
 *       "edit" = "Drupal\batch_system\Form\IngredientForm",
 *       "delete" = "Drupal\batch_system\Form\IngredientDeleteForm",
 *     },
 *     "access" = "Drupal\batch_system\IngredientAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\batch_system\IngredientHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "ingredient",
 *   admin_permission = "administer ingredient entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "description" = "description",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/batch_system/ingredient/{ingredient}",
 *     "add-form" = "/admin/structure/batch_system/ingredient/add",
 *     "edit-form" = "/admin/structure/batch_system/ingredient/{ingredient}/edit",
 *     "delete-form" = "/admin/structure/batch_system/ingredient/{ingredient}/delete",
 *     "collection" = "/admin/structure/batch_system/ingredient",
 *   },
 *   field_ui_base_route = "ingredient.settings"
 * )
 */
class Ingredient extends ContentEntityBase implements BatchSystemInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Ingredient.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Ingredient description.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
