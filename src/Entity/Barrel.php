<?php

namespace Drupal\batch_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\batch_system\Entity\BatchSystemInterface;

/**
 * Defines the Barrel entity.
 *
 * @ingroup batch_system
 *
 * @ContentEntityType(
 *   id = "barrel",
 *   label = @Translation("Barrel"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\batch_system\BarrelListBuilder",
 *     "views_data" = "Drupal\batch_system\Entity\BarrelViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\batch_system\Form\BarrelForm",
 *       "add" = "Drupal\batch_system\Form\BarrelForm",
 *       "edit" = "Drupal\batch_system\Form\BarrelForm",
 *       "delete" = "Drupal\batch_system\Form\BarrelDeleteForm",
 *     },
 *     "access" = "Drupal\batch_system\BarrelAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\batch_system\BarrelHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "barrel",
 *   admin_permission = "administer barrel entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "description" = "description",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/batch_system/barrel/{barrel}",
 *     "add-form" = "/admin/structure/batch_system/barrel/add",
 *     "edit-form" = "/admin/structure/batch_system/barrel/{barrel}/edit",
 *     "delete-form" = "/admin/structure/batch_system/barrel/{barrel}/delete",
 *     "collection" = "/admin/structure/batch_system/barrel",
 *   },
 *   field_ui_base_route = "barrel.settings"
 * )
 */
class Barrel extends ContentEntityBase implements BatchSystemInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Barrel.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Barrel description.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
