<?php

namespace Drupal\batch_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the BarrelLot entity.
 *
 * @ingroup batch_system
 *
 * @ContentEntityType(
 *   id = "barrel_lot",
 *   label = @Translation("BarrelLot"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\batch_system\BarrelLotListBuilder",
 *     "views_data" = "Drupal\batch_system\Entity\BarrelLotViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\batch_system\Form\BarrelLotForm",
 *       "add" = "Drupal\batch_system\Form\BarrelLotForm",
 *       "edit" = "Drupal\batch_system\Form\BarrelLotForm",
 *       "delete" = "Drupal\batch_system\Form\BarrelLotDeleteForm",
 *     },
 *     "access" = "Drupal\batch_system\BarrelLotAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\batch_system\BarrelLotHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "barrel_lot",
 *   admin_permission = "administer barrel_lot entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "description" = "description",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/batch_system/barrel_lot/{barrel_lot}",
 *     "add-form" = "/admin/structure/batch_system/barrel_lot/add",
 *     "edit-form" = "/admin/structure/batch_system/barrel_lot/{barrel_lot}/edit",
 *     "delete-form" = "/admin/structure/batch_system/barrel_lot/{barrel_lot}/delete",
 *     "collection" = "/admin/structure/batch_system/barrel_lot",
 *   },
 *   field_ui_base_route = "barrel_lot.settings"
 * )
 */
class BarrelLot extends ContentEntityBase implements BatchSystemInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the BarrelLot.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('BarrelLot description.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
