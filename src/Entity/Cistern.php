<?php

namespace Drupal\batch_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Cistern entity.
 *
 * @ingroup batch_system
 *
 * @ContentEntityType(
 *   id = "cistern",
 *   label = @Translation("Cistern"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\batch_system\CisternListBuilder",
 *     "views_data" = "Drupal\batch_system\Entity\CisternViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\batch_system\Form\CisternForm",
 *       "add" = "Drupal\batch_system\Form\CisternForm",
 *       "edit" = "Drupal\batch_system\Form\CisternForm",
 *       "delete" = "Drupal\batch_system\Form\CisternDeleteForm",
 *     },
 *     "access" = "Drupal\batch_system\CisternAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\batch_system\CisternHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "cistern",
 *   admin_permission = "administer cistern entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "sample_id", "sample_id",
 *     "cistern_id", "cistern_id",
 *     "fermentor_id", "fermentor_id",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/batch_system/cistern/{cistern}",
 *     "add-form" = "/admin/structure/batch_system/cistern/add",
 *     "edit-form" = "/admin/structure/batch_system/cistern/{cistern}/edit",
 *     "delete-form" = "/admin/structure/batch_system/cistern/{cistern}/delete",
 *     "collection" = "/admin/structure/batch_system/cistern",
 *   },
 *   field_ui_base_route = "cistern.settings"
 * )
 */
class Cistern extends ContentEntityBase implements BatchSystemInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Lot'))
      ->setDescription(t('The name of the Cistern Lot.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['sample_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Analysis Samples'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings(array(
        'target_type' => 'sample',
      ))
      ->setDescription(t('Samples for Analysis.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'above',
        'type' => 'entity_reference_autocomplete',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['cistern_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Cistern Lots'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings(array(
        'target_type' => 'cistern',
      ))
      ->setDescription(t('Other cistern lots that went into this lot.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'above',
        'type' => 'entity_reference_autocomplete',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['fermentor_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Fermentor Lots'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings(array(
        'target_type' => 'fermentor',
      ))
      ->setDescription(t('Fermentor lots that went into this lot.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'above',
        'type' => 'entity_reference_autocomplete',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['distillation_proof'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Distillation Proof'))
      ->setDescription(t('The proof of the distillation batch.'))
      ->setSettings(array(
        'precision' => 6,
        'scale' => 3,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'decimal',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
