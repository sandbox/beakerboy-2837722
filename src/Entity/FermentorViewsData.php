<?php

namespace Drupal\batch_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Fermentor entities.
 */
class FermentorViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['fermentor']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Fermentor'),
      'help' => $this->t('The Fermentor ID.'),
    );

    return $data;
  }

}
