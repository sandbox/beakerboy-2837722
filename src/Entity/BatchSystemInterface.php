<?php

namespace Drupal\batch_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining BatchSystem entities.
 *
 * @ingroup batch_system
 */
interface BatchSystemInterface extends ContentEntityInterface {

  /**
   * Gets the name.
   *
   * @return string
   *   Name of the Entity.
   */
  public function getName();

  /**
   * Sets the name.
   *
   * @param string $name
   *   The Entity name.
   *
   * @return \Drupal\batch_system\Entity\BatchSystemInterface
   *   The called entity.
   */
  public function setName($name);

}
