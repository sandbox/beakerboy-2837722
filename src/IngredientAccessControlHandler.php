<?php

namespace Drupal\batch_system;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Access controller for the Ingredient entity.
 *
 * @see \Drupal\batch_system\Entity\Ingredient.
 */
class IngredientAccessControlHandler extends EntityAccessControlHandler {

}
