<?php

namespace Drupal\batch_system;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Access controller for the Cistern entity.
 *
 * @see \Drupal\batch_system\Entity\Cistern.
 */
class CisternAccessControlHandler extends EntityAccessControlHandler {

}
