<?php

namespace Drupal\batch_system;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Access controller for the Oak entity.
 *
 * @see \Drupal\batch_system\Entity\Oak.
 */
class OakAccessControlHandler extends EntityAccessControlHandler {

}
