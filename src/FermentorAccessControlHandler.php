<?php

namespace Drupal\batch_system;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Access controller for the Fermentor entity.
 *
 * @see \Drupal\batch_system\Entity\Fermentor.
 */
class FermentorAccessControlHandler extends EntityAccessControlHandler {

}
