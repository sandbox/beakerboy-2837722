<?php

namespace Drupal\batch_system;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Access controller for the Barrel entity.
 *
 * @see \Drupal\batch_system\Entity\Barrel.
 */
class BarrelAccessControlHandler extends EntityAccessControlHandler {

}
