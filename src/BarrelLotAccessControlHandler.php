<?php

namespace Drupal\batch_system;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Access controller for the BarrelLot entity.
 *
 * @see \Drupal\batch_system\Entity\BarrelLot.
 */
class BarrelLotAccessControlHandler extends EntityAccessControlHandler {

}
