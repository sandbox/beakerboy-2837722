<?php

namespace Drupal\batch_system;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Access controller for the Recipe entity.
 *
 * @see \Drupal\batch_system\Entity\Recipe.
 */
class RecipeAccessControlHandler extends EntityAccessControlHandler {

}
