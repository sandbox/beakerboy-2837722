<?php

namespace Drupal\batch_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Oak entities.
 *
 * @ingroup batch_system
 */
class OakDeleteForm extends ContentEntityDeleteForm {


}
