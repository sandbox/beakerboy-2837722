<?php

namespace Drupal\batch_system\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for PreviousContents edit forms.
 *
 * @ingroup batch_system
 */
class PreviousContentsForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\batch_system\Entity\PreviousContents */
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label PreviousContents.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label PreviousContents.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.previous_contents.canonical', ['previous_contents' => $entity->id()]);
  }

}
