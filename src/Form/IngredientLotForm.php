<?php

namespace Drupal\batch_system\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for IngredientLot edit forms.
 *
 * @ingroup batch_system
 */
class IngredientLotForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\batch_system\Entity\IngredientLot */
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label IngredientLot.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label IngredientLot.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.ingredient_lot.canonical', ['ingredient_lot' => $entity->id()]);
  }

}
