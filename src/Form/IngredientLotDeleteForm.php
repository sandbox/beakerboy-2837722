<?php

namespace Drupal\batch_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting IngredientLot entities.
 *
 * @ingroup batch_system
 */
class IngredientLotDeleteForm extends ContentEntityDeleteForm {


}
