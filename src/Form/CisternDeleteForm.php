<?php

namespace Drupal\batch_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cistern entities.
 *
 * @ingroup batch_system
 */
class CisternDeleteForm extends ContentEntityDeleteForm {


}
