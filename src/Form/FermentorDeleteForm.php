<?php

namespace Drupal\batch_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Fermentor entities.
 *
 * @ingroup batch_system
 */
class FermentorDeleteForm extends ContentEntityDeleteForm {


}
