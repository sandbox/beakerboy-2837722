<?php

namespace Drupal\batch_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Barrel entities.
 *
 * @ingroup batch_system
 */
class BarrelDeleteForm extends ContentEntityDeleteForm {


}
