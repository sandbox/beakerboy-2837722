<?php

namespace Drupal\batch_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting PreviousContents entities.
 *
 * @ingroup batch_system
 */
class PreviousContentsDeleteForm extends ContentEntityDeleteForm {


}
