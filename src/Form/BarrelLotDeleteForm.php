<?php

namespace Drupal\batch_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting BarrelLot entities.
 *
 * @ingroup batch_system
 */
class BarrelLotDeleteForm extends ContentEntityDeleteForm {


}
