<?php

namespace Drupal\batch_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Recipe entities.
 *
 * @ingroup batch_system
 */
class RecipeDeleteForm extends ContentEntityDeleteForm {


}
