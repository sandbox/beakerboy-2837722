<?php

namespace Drupal\batch_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ingredient entities.
 *
 * @ingroup batch_system
 */
class IngredientDeleteForm extends ContentEntityDeleteForm {


}
