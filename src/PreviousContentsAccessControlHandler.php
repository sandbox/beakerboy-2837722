<?php

namespace Drupal\batch_system;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Access controller for the PreviousContents entity.
 *
 * @see \Drupal\batch_system\Entity\PreviousContents.
 */
class PreviousContentsAccessControlHandler extends EntityAccessControlHandler {

}
