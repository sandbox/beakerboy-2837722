<?php

/**
 * @file
 * Contains previous_contents.page.inc.
 *
 * Page callback for PreviousContents entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for PreviousContents templates.
 *
 * Default template: previous_contents.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_previous_contents(array &$variables) {

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
