<?php

/**
 * @file
 * Contains fermentor.page.inc.
 *
 * Page callback for Fermentor entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Fermentor templates.
 *
 * Default template: fermentor.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_fermentor(array &$variables) {

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
